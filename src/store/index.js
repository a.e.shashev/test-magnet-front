import { createStore } from "vuex";
import baseApi from "@/api/baseApi";
import { DateTime } from "luxon";

export default createStore({
  state: {
    isEditMode: false,
    taskDetails: {
      id: "",
      name: "",
      deadline: "",
      details: "",
      status: "Создание",
      docs: [],
      comments: [],
      docsForUpload: [],
    },
  },
  getters: {},
  mutations: {
    updateName(state, name) {
      state.taskDetails.name = name;
    },
    updateDeadline(state, deadline) {
      state.taskDetails.deadline = deadline;
    },
    updateDetails(state, details) {
      state.taskDetails.details = details;
    },
    updateStatus(state, status) {
      state.taskDetails.status = status;
    },
    updateStep2ForUpload(state, files) {
      state.taskDetails.docsForUpload = files;
    },
    updateStep2Docs(state, additional) {
      state.taskDetails.docs = additional;
    },
    updateStep3(state, comments) {
      state.taskDetails.comments = comments;
    },
    updateAll(state, payload) {
      state.taskDetails.id = payload.id;
      state.taskDetails.name = payload.name;
      state.taskDetails.deadline = DateTime.fromSeconds(
        payload.deadline
      ).toFormat("y-LL-dd");
      state.taskDetails.details = payload.details;
      state.taskDetails.status = payload.status;
      state.taskDetails.comments = payload.comments;
      state.taskDetails.docs = payload.documents;
    },
    reset(state) {
      state.taskDetails.name = "";
      state.taskDetails.details = "";
      state.taskDetails.deadline = "";
      state.taskDetails.status = "Создание";
      state.taskDetails.docs = [];
      state.taskDetails.comments = [];
    },
    changeEditMode(state, status) {
      state.isEditMode = status;
    },
  },
  actions: {
    getTask({ commit }, id) {
      commit("reset");
      return baseApi.get("/task/" + id, {}, {}).then((res) => {
        commit("updateAll", res.data);
        return res;
      });
    },
    saveTask({ commit }) {
      const payload = {
        // Формируем пейлоад для обновления/создания таска
        name: this.state.taskDetails.name,
        details: this.state.taskDetails.details,
        deadline: this.state.taskDetails.deadline,
        status: this.state.taskDetails.status,
      };

      // Формируем пейлоад для обновления/создания документов
      const headers = {
        "Content-Type": "multipart/form-data",
      };
      const docPayload = new FormData();
      if (this.state.taskDetails.docsForUpload.length > 0) {
        this.state.taskDetails.docsForUpload.forEach((doc) => {
          docPayload.append("file", doc);
        });
        docPayload.append("_method", "PUT");
      }
      ////////////////////////////////////////////////////////
      if (this.state.isEditMode) {
        // Проверяем режим работы View, от этого зависит сохраняем или создаем новый
        return baseApi
          .put("/task/" + this.state.taskDetails.id, payload, {})
          .then((res) => {
            if (this.state.taskDetails.docsForUpload.length > 0) {
              docPayload.append("taskId", this.state.taskDetails.id);
              baseApi
                .post("/task/" + res.data.id + "/document", docPayload, headers)
                .then(() => {
                  commit("reset");
                });
            }
          });
      } else {
        return baseApi.post("/task/", payload, {}).then((res) => {
          if (res.data.id) {
            // Если таск создался, то создаем другие сущности, иначе ничего.
            const taskId = res.data.id;

            if (this.state.taskDetails.docsForUpload.length > 0) {
              // Отправляем документы
              docPayload.append("taskId", taskId);
              baseApi
                .post("/task/" + res.data.id + "/document", docPayload, headers)
                .then(() => {});
            }

            if (this.state.taskDetails.comments.length > 0) {
              // Отправляем комменты
              this.state.taskDetails.comments.forEach((comment) => {
                const commentPayload = { text: comment.text };
                baseApi
                  .post("/task/" + res.data.id + "/comment", commentPayload, {})
                  .then(() => {});
              });
            }
          }
        });
      }
    },
  },
  modules: {},
});
