import { createRouter, createWebHistory } from "vue-router";
import TasksView from "../views/TasksView.vue";
import TaskDetails from "@/views/TaskDetails.vue";
import TaskStep1 from "@/components/TaskStep1.vue";
import TaskStep2 from "@/components/TaskStep2.vue";
import TaskStep3 from "@/components/TaskStep3.vue";

const routes = [
  {
    path: "/",
    name: "tasks",
    component: TasksView,
  },
  {
    path: "/task/edit/:taskId",
    name: "taskEdit",
    redirect: () => {
      return {
        name: "TaskEditStep1",
        // params: params.taskId,
      };
    },
    component: TaskDetails,
    children: [
      {
        name: "TaskEditStep1",
        path: "step1", //selected tab by default
        component: TaskStep1,
      },
      {
        name: "TaskEditStep2",
        path: "step2",
        component: TaskStep2,
      },
      {
        name: "TaskEditStep3",
        path: "step3",
        component: TaskStep3,
      },
    ],
  },
  {
    path: "/task/create/",
    name: "taskCreate",
    redirect: () => ({ name: "TaskStep1" }),
    component: TaskDetails,
    children: [
      {
        name: "TaskStep1",
        path: "step1", //selected tab by default
        component: TaskStep1,
      },
      {
        name: "TaskStep2",
        path: "step2",
        component: TaskStep2,
      },
      {
        name: "TaskStep3",
        path: "step3",
        component: TaskStep3,
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
