import axios from "axios";

const token = () => localStorage.getItem("accessToken");
const baseURL = process.env.VUE_APP_API_URL;
const debug = process.env.VUE_APP_DEBUG === "true";

const defaultOptions = {
  baseURL,
  reqHandleFunc: (config) => {
    const accessToken = token();
    if (accessToken) {
      // eslint-disable-next-line no-param-reassign
      config.headers["x-access-token"] = accessToken;
    }

    return config;
  },
  reqErrorFunc: (error) => {
    if (debug) {
      console.error(error);
    }

    return Promise.reject(error);
  },
  resHandleFunc: (response) => response,
  resErrorFunc: (error) => {
    if (debug) {
      console.error(error);
    }

    if (
      error?.response?.status === 401 &&
      window.location.pathname.indexOf("sign-in") === -1
    ) {
      window.location.href = "/auth/sign-out";
    }

    return Promise.reject(error);
  },
};

const initOptions = {
  ...defaultOptions,
};

const service = axios.create(initOptions);

service.interceptors.request.use(
  (config) => initOptions.reqHandleFunc(config),
  (error) => initOptions.reqErrorFunc(error)
);

service.interceptors.response.use(
  (response) => initOptions.resHandleFunc(response),
  (error) => initOptions.resErrorFunc(error)
);

const action = (opt) => service(opt);

export default {
  /**
   * @param {string} url
   * @param {object} data
   * @param {object} options
   */
  get: (url, data, options) =>
    action({
      ...options,
      ...{
        method: "get",
        url,
        params: data,
      },
    }),
  /**
   * @param {string} url
   * @param {object} data
   * @param {object} options
   */
  post: (url, data, options) =>
    action({
      ...options,
      ...{
        method: "post",
        url,
        data,
      },
    }),
  /**
   * @param {string} url
   * @param {object} data
   * @param {object} options
   */
  put: (url, data, options) =>
    action({
      ...options,
      ...{
        method: "put",
        url,
        data,
      },
    }),
  /**
   * @param {string} url
   * @param {object} data
   * @param {object} options
   */
  patch: (url, data, options) =>
    action({
      ...options,
      ...{
        method: "patch",
        url,
        data,
      },
    }),
  /**
   * @param {string} url
   * @param {object} data
   * @param {object} options
   */
  delete: (url, data, options) =>
    action({
      ...options,
      ...{
        method: "delete",
        url,
        data,
      },
    }),
};
