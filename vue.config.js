const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
  devServer: {
    port: 3001,
    proxy: {
      "": {
        target: process.env.VUE_APP_PROXY_HOST,
        secure: false,
        changeOrigin: true,
        ws: true,
      },
    },
  },
});
